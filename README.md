# RADAR

RADAR (**R**est **A**PI **D**ocumentation **A**pplication in **R**eact) is a React/Redux frontend served as a static app.

The aim of this project is to have a UI for searching, browsing and viewing REST documentation for any product and any version of a product. 
It is designed with large APIs in mind, where a single scrolling page of endpoints becomes too unwieldly.

## How to 

Before you do anything: `npm install`
This README assumes you are running a *nix machine.

### Test

- Running unit tests: `npm test`

### Develop

- To start the application: 
    - `npm start` - You'll need to run 2 servers for development, this command does the following:
        - `npm run develop-webpack` - Webpack dev server - this handles hot reloading the JS 
        - `npm run develop-express` - Express dev server - this handles compiling the templates and baking in the test data
    
    - Go to `http://localhost:8080/docs/bitbucket/latest/` <- This will load the `test-data/bitbucket/latest/swagger.json` file.
- You now have hot reloading of JS assets, and automatic rebundling. All without a reload. Cool, huh?
- You can now make changes to any JS or JSX files (or add more). 
  _Note:_ changes to image assets are currently not auto-detected, so if you add or change any, you'll need to run `gulp copy:images`.

_Note:_ `redux-devtools` are installed. You can use `Ctrl+h` to show/hide or `Ctrl+q` to move them. They are useful for time-travel debugging and seeing what's going on in the app state.

### Deploying

We assume you will want to back RADAR with your own server. 
The `server/dev-server.js` file gives some insight into what a RADAR server will need to be able to do. 
RADAR relies on wildcard routing to handle the file-fetching logic server-side, while keeping the routing for resources client-side.

This package only handles rendering a semi-static front end for displaying your documentation. How you store and/or version your docs is entirely up to you.

### Limitations

- JSON only; RADAR currently only supports JSON specs, YAML is not (yet) supported
- Referencing other files is _not_ supported. The entire spec must be in one file.

