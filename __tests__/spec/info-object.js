jest.disableAutomock()
jest.mock('../../src/components/search/search.jsx')

import React from 'react'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import { mockStore } from '../../test-util'

import App from '../../src/containers/app.jsx'
import Overview from '../../src/containers/api-overview/api-overview.jsx'

describe('Info Object', () => {
    const apiInfo = {
        info: {
            title: 'Hello',           
            version: '1'
        }
    };
    
    const apiInfoFull = {
        ...apiInfo,
        info: {
            ...apiInfo.info,
            description: 'Some description for the API',
            termsOfService: 'https://example.com/terms/',
            contact: {
                name: 'Fred Flintstone',
                url: 'https://api.example.com/',
                email: 'fred@example.com'                
            },
            license: {
                name: 'Apache 2.0',
                url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
            }
        }
    };
        
    const requiredStore = mockStore(apiInfo);
    const requiredApp = TestUtils.renderIntoDocument(<App store={requiredStore}><Overview store={requiredStore} /></App>);
    const requiredAppNode = ReactDOM.findDOMNode(requiredApp);
    
    const fullStore = mockStore(apiInfoFull);
    const fullApp = TestUtils.renderIntoDocument(<App store={fullStore}><Overview store={fullStore} /></App>);
    const fullAppNode = ReactDOM.findDOMNode(fullApp);
    
    it('renders the title in the header', () => {               
        const title = requiredAppNode.querySelector('.aui-page-header-main h2');
        expect(title.textContent).toBe(apiInfo.info.title);
    });
    
    it('renders the version on the main page', () => {               
        const version = requiredAppNode.querySelector('.api-details .version');
        expect(version.textContent).toBe(apiInfo.info.version);
    });
    
    it('renders the description on the main page', () => {
        const descriptionSelector = '.api-details .description';
        const requiredDescription = requiredAppNode.querySelector(descriptionSelector);
        expect(requiredDescription).toBe(null);        
        
        const fullDescription = fullAppNode.querySelector(descriptionSelector);
        expect(fullDescription.textContent).toBe(apiInfoFull.info.description);
    });
    
    it('renders the terms of service on the main page', () => {
        const tosSelector = '.api-details .details-tos a';
        
        const requiredTos = requiredAppNode.querySelector(tosSelector);
        expect(requiredTos).toBe(null);    
        
        const tos = fullAppNode.querySelector(tosSelector);
        expect(tos.href).toBe(apiInfoFull.info.termsOfService);    
        expect(tos.textContent).toBe('Terms of Service');
    });
    
    describe('Contact Object', () => {
        const detailsSelector = '.api-details';
        const requiredDetails = requiredAppNode.querySelector(detailsSelector);
        const fullDetails = fullAppNode.querySelector(detailsSelector);
        
        it('renders the contact url on the main page', () => {  
            const contactUrlSelector = '.contact-url a';    
            const requiredUrl = requiredDetails.querySelector(contactUrlSelector);      
            expect(requiredUrl).toBe(null);     
            
            const url = fullDetails.querySelector(contactUrlSelector);      
            expect(url.textContent).toBe(apiInfoFull.info.contact.name);     
            expect(url.href).toBe(apiInfoFull.info.contact.url);  
        });
        
        it('renders the contact name on the main page if there is no url', () => {      
            const apiInfo = {
                info: {
                    contact: {
                        name: 'Imagina Name'
                    }
                }
            };
            const store = mockStore(apiInfo);
            const appOverview = TestUtils.renderIntoDocument(<App store={store}><Overview store={store} /></App>);
            const appNode = ReactDOM.findDOMNode(appOverview);                        
            const details = appNode.querySelector(detailsSelector);            
            let name = details.querySelector('.contact-name');      
            expect(name.textContent).toBe(apiInfo.info.contact.name);     
        });
        
        it('renders the contact email on the main page', () => {   
            const contactEmailSelector = '.contact-email a';
            const requiredEmail = requiredDetails.querySelector(contactEmailSelector);         
            expect(requiredEmail).toBe(null);     
            
            const email = fullDetails.querySelector(contactEmailSelector);         
            expect(email.href).toBe('mailto:' + apiInfoFull.info.contact.email);     
            expect(email.textContent).toBe('Contact developer');     
        });
    });  
    
    describe('License Object', () => {      
        it('renders the license name', () => {
            const licenseSelector = '.api-details .license a';
            
            const requiredLicenseLink = requiredAppNode.querySelector(licenseSelector);
            expect(requiredLicenseLink).toBe(null);
            
            const licenseLink = fullAppNode.querySelector(licenseSelector);
            expect(licenseLink.textContent).toBe(apiInfoFull.info.license.name);
            expect(licenseLink.href).toBe(apiInfoFull.info.license.url);
        });
          
        it('renders the license name if there is no url', () => {
            const apiInfo = {
                info: {
                    license: {
                        name: 'License without a url'
                    }
                }
            };
            const store = mockStore(apiInfo);
            const appOverview = TestUtils.renderIntoDocument(<App store={store}><Overview store={store} /></App>);
            const appNode = ReactDOM.findDOMNode(appOverview);            
            const license = appNode.querySelector('.api-details .license');
            expect(license.textContent).toBe(apiInfo.info.license.name);
        });
    });    
});


