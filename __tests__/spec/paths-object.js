jest.disableAutomock()
jest.mock('../../src/components/search/search.jsx')

import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'
import { mockStore } from '../../test-util'

import App from '../../src/containers/app.jsx'
import RestResource from '../../src/containers/rest-resource.jsx'

describe('Paths Object', () => {
    const apiPaths = {
        info: {
            title: 'Example API'
        },
        paths: {
            '/user': {
                'get': {},
                'post': {}, 
                'invalid': {}
            },
            '/admin': {
                'get': {}
            }
        }
    };
        
    const store = mockStore(apiPaths);
    const appRestResource = TestUtils.renderIntoDocument(<Provider store={store}><App><RestResource location={{pathname: "/resource/"}}/></App></Provider>);
    const appNode = ReactDOM.findDOMNode(appRestResource);
    
    describe('path list', () => {
        const resources = appNode.querySelector('.resource-list-table').querySelector('tbody').querySelectorAll('tr');
        
        it('renders the correct number of resources', () => {
            expect(resources.length).toEqual(2);
        });
        
        describe('path row', () => {
            const getColumnsForRow = (rowIndex) => resources[rowIndex].querySelectorAll('td');
        
            it('is correctly ordered', () => {
                expect(getColumnsForRow(0)[0].textContent).toBe('/admin');
                expect(getColumnsForRow(1)[0].textContent).toBe('/user');
            });       
            
            it('shows the available methods', () => {
                const methods = getColumnsForRow(1)[1].querySelectorAll('span');
                expect(methods.length).toEqual(2);
            
                expect(methods[0].textContent).toBe('get');
                expect(methods[1].textContent).toBe('post');
            }); 
        });               
    })
});