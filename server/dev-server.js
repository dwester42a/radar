var compression = require('compression');
var express = require('express');
var fs = require('fs');
var httpProxy = require('http-proxy');
var Radar = require('../index');

var PORT = 8080;

/**
 * RADAR can be configured with a basePath and a contextPath that provides
 * some flexibility when sharing a domain with other services. RADAR can be
 * hosted down in a subdirectory, as opposed to claiming the domain's root
 * directly.
 *
 * The context path is essentially a basePath, but it is only used for routing.
 * basePath is used only as a prefix for static assets.
 */
var basePath = process.env.RADAR_BASE_PATH || '';
var contextPath = process.env.RADAR_CONTEXT_PATH || basePath;

var app = express();
var proxy = httpProxy.createProxyServer();
app.use(compression());
app.use('/images', express.static(Radar.STATIC_PATHS.images));
app.all('/js/*', function (req, res) {
    proxy.web(req, res, {
        // Proxy any JS requests to the webpack dev server 
        target: 'http://localhost:9090/' 
    }); 
});

/**
 * A test endpoint. 
 */
app.get('/docs/:product/:version*', function (req, res, next) {
     var requestObject = {
        product: req.params.product,
        version: req.params.version,
        filename: 'swagger.json'
    };
    var swaggerContent = fs.readFileSync('test-data/' + requestObject.product + '/' + requestObject.version + '/swagger.json', { encoding:'utf-8' });
    var context = contextPath || ('/docs/' + requestObject.product + '/' + requestObject.version);
    Radar.render(swaggerContent, context, {
        basePath: basePath,
        devMode: '<div style="background-color: #205081; color: white"><center>~Dev Mode~</center></div>'        
    }).then(function (html) {        
        res.status(200).send(html);
    }).catch(function(err) {
        if (err) {
            next(err);
            return;
        }
    });
});

/**
 * Any errors logged to the console will be picked up by the 
 */
app.use(function (err, req, res, next) {
    if (err) {
        console.error(err);
    }
    next();
});

/**
 * Catch any proxy errors.
 */
proxy.on('error', function(e) {
    console.error('Could not connect to proxy, please try again...');
});

/**
 * Fall back to a 404 as a catch-all error page.
 * Order is important here. Express requires this to be the last 
 * defined `app.use`, as handlers are called in the order they are 
 * registered in.
 */
app.use(function (req, res, next) {
    if (req.accepts('html')) {
        Radar.notFound().then(function (html) {          
          res.status(404).send(html);
        }).catch(function (err) {
            if (err) {
              console.error(err);
              res.status(500).send();
          }
        });
        return;
    }

    if (req.accepts('json')) {
        res.status(404).send({ error: 'Not found' });
        return;
    }

    res.type('txt').send('Not found');
});

var server = app.listen(PORT);
