'use strict'; 
import './api-details.less'

import React from 'react'
import Markdown from 'react-markdown'

export default class ApiDetails extends React.Component {
    
    render() {
        const { info } = this.props;
        
        const description = info.description ? <div className="description">
                        <Markdown source={info.description} skipHtml={true}/>
                    </div> : null;
        
        const links = [];
        
        if (info.contact) {
            if (info.contact.url) {
                links.push(<li key="contact-url" className="contact-url"><a href={info.contact.url}>{info.contact.name || info.contact.url}</a></li>);
            } else if (info.contact.name) {
                links.push(<li key="contact-name" className="contact-name">{info.contact.name}</li>);                
            }
            
            if (info.contact.email) {
                links.push(<li key="contact-email" className="contact-email"><a href={'mailto:' + info.contact.email}>Contact developer</a></li>);
            }
        }
        
        if (info.termsOfService) {
            links.push(<li key="details-tos" className="details-tos"><a href={info.termsOfService}>Terms of Service</a></li>);
        }
        
        let license = null;
        if (info.license) {
            if (info.license.url) {
                license = <div className="license"><a href={info.license.url}>{info.license.name || info.license.url}</a></div>;
            } else if (info.license.name) {
                license = <div className="license">{info.license.name}</div>;                
            }
        }

        return (<div className="api-details">
                    {description}
                    <div>
                        <ul>
                            {links}
                        </ul>
                    </div>
                    <span className="version">{info.version}</span>
                    {license}
                </div>);       
    }
}