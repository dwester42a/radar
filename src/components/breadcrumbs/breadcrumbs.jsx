'use strict';
import './breadcrumbs.less'

import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

import { splitPath } from '../../reducers/data-reducer'
import { qualifyResourcePath, unqualifyResourcePath } from '../../utils/route-util'

class Breadcrumbs extends React.Component {
    render() {
        const { sections, onSectionSelected } = this.props;
        return (<ol className="aui-nav aui-nav-breadcrumbs">
            {sections.map(function (value, index) {
                if (index === 0) 
                    return null;
                const last = index + 1 === sections.length;
                if (last) {
                    return <li className="selected-crumb" key={value.path}>{value.name}</li>;
                } else {
                    return <li key={value.path}><Link to={qualifyResourcePath(value.path)}>{value.name}</Link></li>;
                }
            })}
        </ol>);
    }
}

function mapStateToProps(state) {
    return {
        sections: splitPath(unqualifyResourcePath(location.pathname))
    }
}

export default connect(mapStateToProps, {})(Breadcrumbs);
