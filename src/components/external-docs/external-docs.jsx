'use strict';

import React from 'react'
import Markdown from 'react-markdown'

export default class ExternalDocs extends React.Component {
    
    static fromOptionalObject(props) {
        return props ? <ExternalDocs {...props}/> : null;
    }
    
    render() {
        const { description, url } = this.props;
        return <div className="external-docs">
                <a href={url} target="_blank">
                    {description ? <Markdown source={description} skipHtml={true}/> :
                        'More information...'}
                </a>
       		</div>;
    }
}