'use strict';
import React from 'react'

import Search from './search/search.jsx'

export default class Header extends React.Component {
    
    render() {
        const { title, suggestions, location } = this.props;
        return <header className="aui-page-header">
            <div className="aui-page-header-inner">
                <div className="aui-page-header-main">
                    <h2>{title}</h2>
                </div>
                <div className="aui-page-header-actions search-container">
                    <Search suggestions={suggestions} location={location}/>
                </div>
            </div>
        </header>;
    }
}
