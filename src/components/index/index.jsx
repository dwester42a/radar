'use strict';
import './index.less'

import _ from 'lodash'
import React from 'react'

import { Link } from 'react-router'
import { qualifyResourcePath } from '../../utils/route-util'

export default class Index extends React.Component {

    static _toTree(paths) {
        let groups = {};

        function updateObject(object, newValue, stack) {
            while (stack.length > 1) {
                object = object[stack.shift()];
            }

            let accessor = stack.shift();
            if (accessor !== null && accessor !== undefined && !object[accessor]) {
                object[accessor] = newValue;
            }
        }

        Object.keys(paths).forEach(path => {
            let pathComponents = path.split('/');
            let previous = [];
            pathComponents.forEach(component => {
                if (component) {
                    previous.push(component);
                    updateObject(groups, {}, previous.slice());
                }
            });
        });

        return groups;
    }
    
    treeView(treeObject) {
        const selectedPath = this.props.path;
        let createTree = function (key, root, path) {
            const children = [];
            const currentPath = (path ? path : '') + '/' + key;
            if ((selectedPath + '/').indexOf(currentPath + '/') === 0) {
                const keys = root ? Object.keys(root).sort() : [];
                for (let key of keys) {
                    children.push(createTree(key, root[key], currentPath));    
                }
            }
            
            let link = null;
            if (key !== null) {
                if (currentPath === selectedPath) {
                    link = <a className="aui-nav-selected">{key}</a>;
                } else {
                    link = <Link to={qualifyResourcePath(currentPath)}>{key}</Link>;
                }
            }
            return (<li key={key}>
                    {link}
                    <ul>
                        {children}
                    </ul>
                </li>);
        }
        
        return Object.keys(treeObject).sort().map(key => createTree(key, treeObject[key]));        
    }

    render() {
        const { paths } = this.props;
        const tree = Index._toTree(paths);
        return (<nav className="aui-navgroup aui-navgroup-vertical">
            <div className="aui-navgroup-inner">                
                <div className="aui-nav-heading">
                    <strong>Resources</strong>
                </div>
                <ul className="aui-nav">
                    {this.treeView(tree)}
                </ul>
            </div>
        </nav>);
    }
}
