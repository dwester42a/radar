'use strict'; 
import './description.less'

import React from 'react'
import Markdown from 'react-markdown'

export default class Description extends React.Component {
    
    render() {
        const { title, key, html } = this.props;
        if (!html || html.length === 0) {
            return null;
        }     
        
        let titleEl = title ? <h3 id={title}>{title}</h3> : null;
        return (
            <div key={key} className="resource-description">
                {titleEl}
                <Markdown source={html} skipHtml={true}/>
            </div>);
    }
}