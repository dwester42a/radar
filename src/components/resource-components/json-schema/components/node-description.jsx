'use strict';

import React from 'react'
import Markdown from 'react-markdown'

export default class NodeDescription extends React.Component {
    
    render() {
        const { value } = this.props;
        if (!value) {
            return null;
        }
        return <span className="node-description"><Markdown source={value} skipHtml={true}/></span>
    }
} 