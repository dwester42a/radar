'use strict';

import React from 'react'

export default class NodeRequired extends React.Component {
    
    render() {
        const { required } = this.props;
        if (!required) {
            return null;
        }
        return <span className="node-required"></span>;
    }
} 