'use strict';

import React from 'react'

export default class NodeTitle extends React.Component {
    
    render() {
        const { title } = this.props;
        if (!title) {
            return null;
        }
        return <span className="node-title">{title}</span>;
    }
} 