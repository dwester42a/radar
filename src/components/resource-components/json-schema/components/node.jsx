'use strict';

import React from 'react'

export default class Node extends React.Component {
    
    render() {
        const { className, children } = this.props;
        return <div className={'schema-node ' + className}>
                {children}
            </div>;
    }
} 