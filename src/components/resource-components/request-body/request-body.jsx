'use strict'; 
import './request-body.less'

import React from 'react'
import Highlight from 'react-highlight'

import Example from '../example.jsx'
import JsonSchema from '../json-schema/json-schema.jsx'

export default class RequestBody extends React.Component {
    
    render() {
        const { body } = this.props;
      
        if (body && body.schema) {
            let result;
            if (body.schema.example) {
                result = <Example value={body.schema.example}/>
            } else {
                result = <JsonSchema data={body.schema} hideReadOnly={true}/>
            }

            return <div className="resource-request resource-section">
                <h4>Request example</h4>
                {result}
            </div>
        } else {
            return null;
        }
    }
}