'use strict'; 
import './responses.less'

import React from 'react'
import Markdown from 'react-markdown'

import Example from '../example.jsx'
import JsonSchema from '../json-schema/json-schema.jsx'


export default class Responses extends React.Component {
    
    render() {
        const { resource } = this.props;
        if (resource.responses) {
            return (<div className="resource-section">
                <h4>Responses</h4>
                <table className="aui resource-responses">
                    <thead>
                    <tr>
                        <th key="status" className="status">Status Code</th>
                        <th key="desc" className="description">Description</th>
                        <th key="example" className="example">Example</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(resource.responses).map(responseCode => {
                        let examples;
                        if (resource.responses[responseCode].schema) {
                            examples = <JsonSchema key={responseCode + '-schema'} data={resource.responses[responseCode].schema} />
                        } else if (resource.produces) {
                            examples = resource.produces.map(contentType => {
                                if (resource.responses[responseCode].examples) {
                                    let src = resource.responses[responseCode].examples[contentType];
                                    return <Example key={contentType + '-example'} value={src}/>
                                }
                            });
                        }
                        return (
                            <tr key={responseCode}>
                                <td key="code">{responseCode}</td>
                                <td key="desc"><Markdown source={resource.responses[responseCode].description} skipHtml={true}/></td>
                                <td key="example">
                                    {examples}
                                </td>
                            </tr>);
                    })}
                    </tbody>
                </table>
            </div>);
        } else {
            return null;
        }
    }
}