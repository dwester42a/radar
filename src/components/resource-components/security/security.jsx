'use strict'; 
import _ from 'lodash'
import React from 'react'

export default class Security extends React.Component {
       
    static scope(value, descriptions) {
        const description = descriptions[value];
        if (description !== undefined) {
            return <span key={value}><code>{value}</code><span> - </span>{description}</span>;
        } else {
            return value;
        }
    }
    
    static scopes(value, scopes, definitions) {
        if (scopes.length > 0) {
            return (<ul key={value}>
                    {scopes.map(scope => <li key={scope}>{Security.scope(scope, definitions[value]['scopes'])}</li>)}
                </ul>);
        } else {
            return null;
        }
    }
    
    render() {
        const { definitions, security } = this.props;
        if (security) {
            const scopes = _.compact(security.map(value => {
                        const key = Object.keys(value)[0];
                        return Security.scopes(key, value[key], definitions);
                    }));

            if (scopes.length === 0) {
                return null;
            }
            
            return (<div className="resource-section">
                <h4>Required Scopes</h4>
                {scopes}
            </div>)
        } else {
            return null;
        } 
    }    
}