'use strict';
import './search.less'

import React from 'react'
import Autosuggest from 'react-autosuggest'

import { getHistory } from '../../utils/route-util'

export default class Search extends React.Component {
       
	onSearchChange(event, { newValue, method }) {
        const { location } = this.props;

        if (newValue !== '') {                  
            if (location.query.q && location.query.q.endsWith(':') || location.pathname.indexOf('/search') !== 0) {   
                getHistory().push({
                    pathname: '/search',
                    query: { q: newValue } 
                });
            } else {   
                getHistory().replace({
                    pathname: '/search',
                    query: { q: newValue } 
                });
            }
        } else {
            getHistory().push('/');
        }
	}
    
    renderSuggestion(suggestion) {
        return (
            <div>
                <span className="title">{suggestion.title}</span>
                <span className="description">{suggestion.description}</span>
            </div>);
    }
    
    getSuggestionValue(suggestion) {
        return suggestion.query;
    }

	render() {
        const { location }  = this.props;
        const inputProps = {
            placeholder: 'Search...',
            value: location.query.q || '',
            onChange: this.onSearchChange.bind(this)
        }
		return (<Autosuggest  suggestions={this.props.suggestions}
                              shouldRenderSuggestions={(value) => value.length === 0}
                              inputProps={inputProps}
                              renderSuggestion={this.renderSuggestion}
                              getSuggestionValue={this.getSuggestionValue}/>);
	}

}