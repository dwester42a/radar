'use strict';
import './api-overview.less'

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { linkWithBase } from '../../utils/basepath-util'

import ApiDetails from '../../components/api-details/api-details.jsx'
import ExternalDocs from '../../components/external-docs/external-docs.jsx'
import Page from '../../components/aui/page/page.jsx'
import ImageLink from '../../components/image-link/image-link.jsx'

class Overview extends Component {      
        
    render() {
        const { data } = this.props;
        
        const imageLinks = [
            <ImageLink  key="resource-image-link"
                        to="/resource/" 
                        image={linkWithBase("/images/explore.png")}
                        title="Browse" 
                        description="Explore the API for information on methods, endpoints" />
        ];
        
        if (data.tags) {
            imageLinks.push(<ImageLink  key="tag-image-link"
                                        to="/search?q=tag:" 
                                        image={linkWithBase("/images/tags.png")}
                                        title="Tags" 
                                        description="Exploring by tag helps you hone in on the endpoints relevant to you" />);
        }
        
        if (data.securityDefinitions) {
            imageLinks.push(<ImageLink  key="security-image-link"
                                        to="/security" 
                                        image={linkWithBase("/images/security.png")}
                                        title="Security" 
                                        description="Find out what Authentication and Authorization methods this API supports" />);
        }
        
        return (<Page>
                    <div className="image-links-centered">
                        {imageLinks}
                    </div>
                    <div className="api-overview-sections">
                        {ExternalDocs.fromOptionalObject(data.externalDocs)}
                        <ApiDetails info={data.info}/>
                    </div>
                </Page>);
    }
}

export default connect(s => s, {})(Overview);