import React, { Component } from 'react'
import App from '../app.jsx'
import DevTools from './dev-tools.jsx'

export default class Root extends Component {
  render() {
      const { children, location } = this.props;
    return (<div>
                <App children={children} location={location}/>
                <DevTools/>
            </div>);
  }
}