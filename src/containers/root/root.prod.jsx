import React, { Component } from 'react'
import App from '../app.jsx'

export default class Root extends Component {
  render() {
    const { children, location } = this.props;
    return (<div>
                <App children={children} location={location} />
            </div>);
  }
}