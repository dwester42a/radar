'use strict';
import './security-overview.less'

import React from 'react'
import { connect } from 'react-redux'

import Page from '../../components/aui/page/page.jsx'
import SecurityDefinitions from '../../components/security-definitions/security-definitions.jsx'

class SecurityOverview extends React.Component {
    
    render() {
        return (<Page>
            <h2 className="security-title">Security</h2>
            <SecurityDefinitions definitions={this.props.data.securityDefinitions}/>
        </Page>);
    }
}

export default connect(s => s, {})(SecurityOverview);