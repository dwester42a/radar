'use strict';
import _ from 'lodash'

import { DATA_LOAD } from '../action-creators/data-action-creators'

export function filterPaths(paths, query) {
    let queryParts = _.compact(query.split(' '));
    return _.omitBy(paths, (value, key) => _.some(queryParts, (part) => key.indexOf(part) === -1));
}

export function filterPathsByTag(paths, query) {
    return _.omitBy(paths, (path, key) => {
        for (let method in path) {
            if (path[method].tags && path[method].tags.indexOf(query) !== -1) {
                return false;
            }
        }
        return true;
    });
}

export function filterPathsByScope(paths, query) {
    return _.omitBy(paths, (path, key) => {
        for (let method in path) {
            if (path[method].security) {
                const oAuthObj = _.find(path[method].security, function(o) { return o['oauth2'] }, {});
                if (oAuthObj['oauth2'].indexOf(query) !== -1) {
                    return false;
                }
            }            
        }
        return true;
    });
}

export function splitPath(selectedPath) {
    let sections = [];
    selectedPath.split('/').forEach(section => {
        sections.push({
            key: section,
            name: section,
            path: sections.map(section => section.key).join('/') + '/' + section
        });
    });
    return sections;
}

export function getResourcesForPath(path, paths) {
    return {
        resource: paths[path],
        subResources: _.omitBy(paths, (value, key) => {
            return key.indexOf(path + '/') !== 0;
        })
    }
}

export function reducer(state = null, action) {
    switch (action.type) {
        case DATA_LOAD:
            return action.data;
        default:
            return state;
    }
}