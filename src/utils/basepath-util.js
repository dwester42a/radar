'use strict';

let basePath = null;

export function getBasePath() {
    return basePath;
}

export function setBasePath(path) {
    basePath = path;
}

export function linkWithBase(link) {
    return basePath + link;
}
