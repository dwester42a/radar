var _ = require('lodash');
var Webpack = require('webpack');
var path = require('path');

var mainPath = path.resolve(__dirname, '..', 'src', 'main.jsx');
var outputPath = path.resolve(__dirname, '..', 'dist', 'js');

/**
 * Dev build config.
 */
module.exports = _.assign({}, require('./webpack.common'), {
    devtool: 'eval',
    entry: [
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:9090',   
        mainPath
    ],
    output: {        
        path: outputPath,
        filename: 'bundle.js',   
        publicPath: '/js/'
    },
    plugins: [
        new Webpack.HotModuleReplacementPlugin(),
    ]
});